from django.urls import path
from . import views

app_name = 'person_app'

urlpatterns = [
    path('', views.InitialView.as_view(), name = 'initial'),
    path('employee-all/', views.ListAllPerson.as_view(), name='employee_all'),
    path('employee-by-area/<shortname>', views.ListByAreaEmployee.as_view(), name='employee_by_area'),
    path('employee-admin/', views.ListPersonAdmin.as_view(), name='employee_admin'),
    path('employee-search/', views.ListEmployeeByKword.as_view()),
    path('employee-skills/', views.ListEmployeeSkills.as_view()),
    path('employee-detail/<pk>/', views.EmployeeDetailView.as_view(), name='employee_detail'),
    path('employee-add/', views.EmployeeCreateView.as_view(), name='employee_add'),
    path('success/', views.SuccessView.as_view(), name = 'success'),
    path('employee-update/<pk>/', views.EmployeeUpdate.as_view(), name = 'employee_update'),
    path('employee-delete/<pk>/', views.EmployeeDeleteView.as_view(), name = 'employee_delete'),
]
