from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import (
    ListView,
    DetailView,
    CreateView,
    TemplateView,
    UpdateView,
    DeleteView
    )
from .models import Person
from .forms import PersonForm

class InitialView(TemplateView):
    template_name = 'index.html'


class ListAllPerson(ListView):
    # Lista de todos los empleados
    template_name = 'person/list_all.html'
    paginate_by = 4

    def get_queryset(self):
        key_word = self.request.GET.get('kword', '')
        list_employee = Person.objects.filter(
            full_name__icontains=key_word
            # icontains sirve para buscar caracteres dentro de una lista de carateres
        )
        print('Keyword:', key_word)
        return list_employee


class ListPersonAdmin(ListView):
    # Lista de todos los empleados
    template_name = 'person/employee-admin.html'
    paginate_by = 10
    context_object_name = 'employees'
    model = Person


class ListByAreaEmployee(ListView):
    # Lista de todos los empleados por area
    template_name = 'person/list_by_area.html'
    context_object_name = 'employees'
    def get_queryset(self):
        area = self.kwargs['shortname']
        list_employee = Person.objects.filter(
            deparment__short_name = area
        )
        return list_employee


class ListEmployeeByKword(ListView):
    # Lista de los empleados por palabra clave
    template_name = 'person/list_by_kword.html'
    context_object_name = 'employees'
    def get_queryset(self):
        print('****')
        key_word = self.request.GET.get('kword', )
        list_employee = Person.objects.filter(
            first_name = key_word
        )
        print('Keyword:', key_word)
        return list_employee


class ListEmployeeSkills(ListView):
    template_name = 'skills.html'
    context_object_name = 'skills'
    def get_queryset(self):
        employee = Person.objects.get(id=8)
        return employee.skill.all()


class EmployeeDetailView(DetailView):
    model = Person
    template_name = 'person/detail_employee.html'


class SuccessView(TemplateView):
    model = Person
    template_name = 'success.html'


class EmployeeCreateView(CreateView):
    template_name = 'person/employee-add.html'
    model = Person
    form_class = PersonForm
    success_url = reverse_lazy('person_app:employee_add')

    def form_valid(self, form):
        employee = form.save(commit=False)
        employee.full_name = employee.first_name + ' ' + employee.last_name
        #print(employee.first_name + ' ' + employee.last_name)
        employee.save()
        return super(EmployeeCreateView, self).form_valid(form)


class EmployeeUpdate(UpdateView):
    template_name = 'person/employee-update.html'
    model = Person
    fields = [
        'first_name',
        'last_name',
        'position',
        'deparment',
        'skill',
        'image' ]
    success_url = reverse_lazy('person_app:employee_admin')

    def post(self, request, *args, **kwargs):
        #guardar datos antes de ser validados
        self.object = self.get_object()
        return super().post(request, *args, **kwargs)

    def form_valid(self, form):
        return super(EmployeeUpdate, self).form_valid(form)


class EmployeeDeleteView(DeleteView):
    model = Person
    template_name = 'person/employee-delete.html'
    success_url = reverse_lazy('person_app:employee_admin')
