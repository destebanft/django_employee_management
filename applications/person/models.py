from django.db import models
from ckeditor.fields import RichTextField
from applications.department.models import Department


class Skills(models.Model):
    skill = models.CharField('Skill', max_length=50)


    class Meta:
        verbose_name = 'Skill'
        verbose_name_plural = 'Employee skills'

    def __str__(self):
        return self.skill

class Person(models.Model):

    POSITIONS = [
        ('EE', 'Electronics Engineer'),
        ('SE', 'Software Engineer'),
        ('ET', 'Electronic Technologist'),
        ('ST', 'Software Technologist'),
    ]

    first_name = models.CharField('Fisrt name', max_length=50)
    last_name = models.CharField('Last name', max_length=50)
    full_name = models.CharField('Full name', max_length=100, blank=True)
    position = models.CharField('Position', max_length=2, choices=POSITIONS)
    deparment = models.ForeignKey(Department, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='employee', blank=True, null=True)
    skill = models.ManyToManyField(Skills)
    curriculum_vitae = RichTextField()




    def __str__(self):
        return self.first_name + ' ' + self.last_name
