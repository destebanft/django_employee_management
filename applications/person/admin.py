from django.contrib import admin
from .models import Person, Skills
# Register your models here.


class EmployeeAdmin(admin.ModelAdmin):
    list_display = [
    'first_name',
    'last_name',
    'deparment',
    'position',
    'full_name',
    ]

    def full_name(self, obj):
        return obj.first_name + ' ' + obj.last_name
        
    search_fields = ('first_name',)
    list_filter = ('position',)
    filter_horizontal = ('skill',)

admin.site.register(Person, EmployeeAdmin)
admin.site.register(Skills)
