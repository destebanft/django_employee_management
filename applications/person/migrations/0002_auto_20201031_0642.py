# Generated by Django 3.1.2 on 2020-10-31 06:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('person', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='position',
            field=models.CharField(choices=[(0, 'Electronics engineer'), (1, 'Software enginer'), (2, 'Electronic Technologist'), (3, 'Software Technologist')], max_length=1, verbose_name='Position'),
        ),
    ]
