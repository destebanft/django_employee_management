from django.shortcuts import render
from django.views.generic import TemplateView, ListView, CreateView
from .models import Prueba
from .forms import PruebaForm

# Create your views here.
class PrubeView(TemplateView):
    template_name = 'home/prueba.html'


class PrubeList(ListView):
    template_name = 'lista.html'
    model = Prueba
    context_object_name = 'ListaLetras'


class PrubeCreate(CreateView):
    template_name = 'create.html'
    model = Prueba
    form_class = PruebaForm
    success_url = None
