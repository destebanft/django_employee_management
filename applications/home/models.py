from django.db import models

# Create your models here.
class Prueba(models.Model):
    title = models.CharField(max_length=30)
    subtitle = models.CharField(max_length=60)
    cant = models.IntegerField()

    def __str__(self):
        return self.title
