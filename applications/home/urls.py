from django.urls import path
from . import views


urlpatterns = [
    path('prueba/', views.PrubeView.as_view()),
    path('lista/', views.PrubeList.as_view()),
    path('create/', views.PrubeCreate.as_view()),
]
