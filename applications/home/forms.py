from django import forms
#from django.core.exceptions import ValidationError
from .models import Prueba


class PruebaForm(forms.ModelForm):


    class Meta:
        model = Prueba
        fields = (
            'title',
            #'subtitle',
            'cant',
        )
        widgets = {
            'title': forms.TextInput(
                attrs = {
                    'placeholder': 'Ingrese el título'
                }
                )
        }
    def clean_cant(self):
        cant = self.cleaned_data['cant']
        if cant < 10:
            raise forms.ValidationError('Please enter a higher number than 10')

        return cant
