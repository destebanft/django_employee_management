from django.db import models

# Create your models here.

class Department(models.Model):
    name = models.CharField('Name', max_length=50)
    short_name = models.CharField('Short name', max_length=15, unique=True)
    state = models.BooleanField('State Department', default=False)

    class Meta:
        verbose_name = 'Department Admin'
        verbose_name_plural = 'Department Administrator'

    def __str__(self):
        return self.short_name
