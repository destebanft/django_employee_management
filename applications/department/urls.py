from django.urls import path
from . import views

app_name = 'department_app'

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('department-new/', views.NewDeparmentView.as_view(), name='department_new'),
    path('department-list/', views.DeparmentListView.as_view(), name='department_list'),
]
