from django.shortcuts import render
from django.views.generic import ListView
from django.views.generic.edit import FormView
from applications.person.models import Person
from .models import Department
from .forms import NewDeparmentForm


class DeparmentListView(ListView):
    model = Department
    template_name = 'department/list.html'
    context_object_name = 'departments'


class NewDeparmentView(FormView):
    template_name = 'new_deparment.html'
    form_class = NewDeparmentForm
    success_url = '/'

    def form_valid(self, form):
        depa = Department(
            name = form.cleaned_data['department'],
            short_name = form.cleaned_data['short_name']
        )
        depa.save()
        name = form.cleaned_data['name']
        last_name = form.cleaned_data['last_name']
        Person.objects.create(
            first_name = name,
            last_name = last_name,
            position = '1',
            deparment = depa
        )


        return super(NewDeparmentView, self).form_valid(form)
